from flask import Flask, app, render_template, request, url_for, redirect
app = Flask(__name__)

@app.route("/", methods=["POST","GET"])
def index():
    if request.method == "POST":
        number = request.form["nm"]
        return redirect(url_for("fact",num=number))
    else:
        return "<form action='#' method='post'><p>How to find a factorial of a number ?</p><input type='number' name='nm'><input type='submit' value='Show'></form>"

@app.route("/fact/<num>")
def fact(num):
    num = int(num)
    ans = 1
    m = num
    for i in range(num):
        ans = ans*m
        m=m-1
        
    return f"<h1> The factorial number {num} is {ans}<h1>"

if __name__ == "__main__":
    app.run(debug=True)